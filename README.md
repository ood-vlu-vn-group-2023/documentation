# documentation



## How to use git (Hướng dẫn sử dụng git)

### Tạo 1 bản sao trong máy (git clone)

Chưa có repo git trong máy local
```
git clone https://gitlab.com/duongdanten/tenrepo
```

Chỉ dùng clone khi

### Nhánh (git branch)

1 repo có thể có nhiều nhánh, tùy thuộc vào việc bạn muốn tạo nhánh đề phòng
việc sai sót hay tạo nhánh tùy theo tính năng

Tạo nhánh
```
git branch test
```

Chuyển nhánh
```
git checkout test
```

Kiểm tra có bao nhiêu nhánh trong repo

```
git branch
```

### Nơi ghi nhớ chỗ lưu code (git remote)

Thường khi clone về sẽ có 1 remote mặc định là `origin` trỏ về máy chủ

Thí vụ muốn thêm remote repo chính thay vì origin repo fork
```
git remote add upstream https://gitlab.com/ood-vlu-vn-group-2023/documentation
```

Để hiện các remote hiện lưu trong repo
```
git remote -v
```

Để xóa remote
```
git remote remove upstream
```

### Đẩy code lên máy chủ (git push)

Thường sẽ đẩy code theo nhánh

Ví dụ đẩy code lên remote origin với nhánh main
```
git push origin main
```

### Lấy dữ liệu (git fetch)

Thường chỉ để lấy dữ liệu từ remote về

Ví dụ lấy dữ liệu từ upstream
```
git fetch upstream
```

### Hợp nhất (git merge)

Có thể dùng để hợp nhất nhánh của remote hoặc nhánh trong repo

Hợp nhất nhánh trong repo
```
git merge main test
```

Hợp nhát nhánh với nhánh của remote
```
git merge main upstream/main
```

### Cập nhập repo (git pull)

Dùng để update repo khi upstream có cập nhập nhưng mình không làm gì cả.
Là câu lệnh tổng hợp các bước của git fetch và git merge.

Nếu như clone từ repo gốc của nhóm thì dùng mỗi lệnh
```
git pull
```

Nhưng nếu là clone từ repo fork thì dùng

```
git pull upstream
```

### FAQ (Hỏi đáp dành cho những thằng quá ngu Git)

Hãy thừa nhận rằng bạn quá ngu, lười biếng và bạn không muốn hiểu thêm nên mới phải
tìm tới giải đáp thắc mắc này.

Q: Tôi quá ngu phải làm sao.

A: Đừng ngu nữa. Hãy tự trau dồi kiến thức để hiểu biết thêm.

Q: Cách sử dụng Git khi tôi quá ngu

A: Hãy tự học hỏi thêm. Và hiểu bản chất cốt lõi của nó.

Q: Nếu tôi muốn đẩy code lên repo fork rồi sau khi repo chính chấp nhận PR của
tôi xong tôi phải làm gì? Tôi muốn repo fork của tôi trùng commit với repo chính.

A: Bạn quá ngu nên hãy bật force push trong protected branch của bạn (tôi không
chịu trách nhiệm cho sự thiếu kiến thức và nếu có sai sót lỗi là ở bạn vì bạn ngu)

Trước hết đẩy lên repo fork của bạn không cần `-f` viết tắt force push
```
git push origin main
```

Sau đó tạo PR chờ cập nhập từ `upstream`. Sau khi `upstream` được cập nhập hãy
chắc chắn rằng bạn có lưu remote của repo chính thay vì repo fork. Dưới đây `upstream`
```
git fetch upstream
```

Giờ hãy reset commit của bạn về như `upstream/main`
```
git reset --hard upstream/main
```

Việc còn lại là đẩy lênh repo fork của bạn với `-f` force push để reset mà không

```
git push origin main -f
```

Đừng ngu học nữa. Đừng nhờ và chờ đợi 1 người nào đó phải chỉ bạn. Sao không phải là bạn
mà phải là người khác?
